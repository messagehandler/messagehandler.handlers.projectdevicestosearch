namespace ProjectDevicesToSearch
{
    public enum DeviceActivityState
    {
        Enabled,
        Revoked
    }
}