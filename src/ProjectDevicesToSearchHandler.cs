﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Reactive.Linq;
using System.Runtime.Caching;
using System.Threading.Tasks;
using MessageHandler;
using MessageHandler.SDK.EventSource;
using Yaus.Framework.AzureSearch;
using Environment = MessageHandler.Environment;

namespace ProjectDevicesToSearch
{
    public class ProjectDevicesToSearchHandler :
        IStandingQuery<DynamicJsonObject>,
        IAction<DynamicJsonObject>
    {
        private readonly AzureSearchClient _searchclient;
        private readonly string _sourceType;
        private readonly string _index;
        private readonly MemoryCache _projections = new MemoryCache("Projections");
        private readonly IInvokeProjections _projectionInvoker;

        public ProjectDevicesToSearchHandler(IConfigurationSource source, IVariablesSource variables, ITemplatingEngine templating, IInvokeProjections projectionInvoker)
        {
            _projectionInvoker = projectionInvoker;

            dynamic channelVariables = variables.GetChannelVariables(Channel.Current());
            dynamic accountVariables = variables.GetAccountVariables(Account.Current());
            dynamic environmentVariables = variables.GetEnvironmentVariables(Environment.Current());

            var config = source.GetConfiguration<ProjectDevicesToSearchConfig>();

            var serviceName = templating.Apply(config.SearchServiceName, null, channelVariables, environmentVariables, accountVariables);
            var apiKey = templating.Apply(config.SearchServiceApiKey, null, channelVariables, environmentVariables, accountVariables);
            _sourceType = templating.Apply(config.SourceType, null, channelVariables, environmentVariables, accountVariables);
            _index = templating.Apply(config.SearchServiceIndex, null, channelVariables, environmentVariables, accountVariables);

            _searchclient = new AzureSearchClient(serviceName, apiKey);
        }

        public IObservable<DynamicJsonObject> Compose(IObservable<DynamicJsonObject> messages)
        {
            return from e in messages
                   where IsDeviceEvent(e)
                   select e;
        }

        private bool IsDeviceEvent(dynamic e)
        {
            string what = e.What;
            string deviceId = e.DeviceId;
            string sourceId = e.SourceId;
            string sourceType = e.SourceType;

            Trace.TraceInformation("Evaluating What:'{0}', DeviceId:'{1}', SourceId:'{2}', SourceType:'{3}'", what, deviceId, sourceId, sourceType);

            return e.What != null && e.DeviceId != null && e.SourceId != null && e.SourceType == _sourceType;
        }

        public async Task Action(DynamicJsonObject t)
        {
            dynamic m = t;

            string deviceId = m.DeviceId;
            string what = m.What;

            Trace.TraceInformation("Received event from device '{0}'.", deviceId);

            var eventType = GetType(what);
            if (eventType == null)
            {
                Trace.TraceInformation("Received event of unknown type '{0}' skipping", what);
                return;
            }

            var @event = Json.Decode(Json.Encode(t), eventType);
            if (@event == null)
            {
                Trace.TraceInformation("Could not deserialize json into claimed type '{0}'", what);
                return;
            }

            var lazy = new Lazy<SearchEntry>(() => _searchclient.Get<SearchEntry>(_index, deviceId).Result ?? new SearchEntry() { Id = deviceId });
            var cachedLazy = (Lazy<SearchEntry>)_projections.AddOrGetExisting(deviceId, lazy, new CacheItemPolicy() { SlidingExpiration = TimeSpan.FromMinutes(1) });
            var device = (cachedLazy ?? lazy).Value;

            Trace.TraceInformation("Retrieved device '{0}', going to apply projection.", deviceId);

            _projectionInvoker.Invoke(device, @event);

            Trace.TraceInformation("Projection applied to device '{0}', persisting.", deviceId);
        }

        private static Type GetType(string typeName)
        {
            var type = Type.GetType(typeName);
            if (type != null) return type;
            foreach (var a in AppDomain.CurrentDomain.GetAssemblies())
            {
                type = a.ExportedTypes.FirstOrDefault(t => t.Name == typeName);
                if (type != null)
                    return type;
            }
            return null;
        }

        public async Task Complete()
        {
            foreach (var pair in _projections)
            {
                var device = ((Lazy<SearchEntry>)pair.Value).Value;
                await _searchclient.Upsert(_index, new object[] { device });
            }
        }

    }
}