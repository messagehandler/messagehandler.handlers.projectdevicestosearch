using MessageHandler;
using MessageHandler.SDK.EventSource;

namespace ProjectDevicesToSearch
{
    public class AddToContainer : IInitialization
    {
        public void Init(IContainer container)
        {
            var source = container.Resolve<IConfigurationSource>();
            var config = source.GetConfiguration<ProjectDevicesToSearchConfig>();
            container.UseEventSourcing(config.ConnectionString)
                    .EnableProjections(config.ConnectionString);
        }
    }
}