using MessageHandler;
using MessageHandler.SDK.EventSource;
using MessageHandler.Contracts.Devices;

namespace ProjectDevicesToSearch
{
    public class Projection :
        IProjection<SearchEntry, DeviceRegistered>,
        IProjection<SearchEntry, DeviceRevoked>,
        IProjection<SearchEntry, DeviceAssociatedToEndpoint>,
        IProjection<SearchEntry, DeviceSecretChanged>
    {
        public void Project(SearchEntry record, DeviceRegistered msg)
        {
            record.Id = msg.Details.DeviceId;
            record.Filter1 = msg.Details.DeviceSecret;
            record.Reference1 = msg.Details.EndpointId;
            record.Title = msg.Details.Name;
            record.State1ValidFrom = msg.Details.ValidFromUtc;
            record.State1ValidTo = msg.Details.ValidToUtc;
            record.State1 = msg.Details.ActivityState.ToString();
            record.Account = msg.Details.Owner;
            record.Object = Json.Encode(msg.Details);
            record.Type = "Device";
        }

        public void Project(SearchEntry record, DeviceRevoked msg)
        {
            record.Id = msg.Details.DeviceId;
            record.Filter1 = msg.Details.DeviceSecret;
            record.Reference1 = msg.Details.EndpointId;
            record.Title = msg.Details.Name;
            record.State1ValidFrom = msg.Details.ValidFromUtc;
            record.State1ValidTo = msg.Details.ValidToUtc;
            record.State1 = msg.Details.ActivityState.ToString();
            record.Account = msg.Details.Owner;
            record.Object = Json.Encode(msg.Details);
            record.Type = "Device";
        }

        public void Project(SearchEntry record, DeviceAssociatedToEndpoint msg)
        {
            record.Id = msg.Details.DeviceId;
            record.Filter1 = msg.Details.DeviceSecret;
            record.Reference1 = msg.Details.EndpointId;
            record.Title = msg.Details.Name;
            record.State1ValidFrom = msg.Details.ValidFromUtc;
            record.State1ValidTo = msg.Details.ValidToUtc;
            record.State1 = msg.Details.ActivityState.ToString();
            record.Account = msg.Details.Owner;
            record.Object = Json.Encode(msg.Details);
            record.Type = "Device";
        }

        public void Project(SearchEntry record, DeviceSecretChanged msg)
        {
            record.Id = msg.Details.DeviceId;
            record.Filter1 = msg.Details.DeviceSecret;
            record.Reference1 = msg.Details.EndpointId;
            record.Title = msg.Details.Name;
            record.State1ValidFrom = msg.Details.ValidFromUtc;
            record.State1ValidTo = msg.Details.ValidToUtc;
            record.State1 = msg.Details.ActivityState.ToString();
            record.Account = msg.Details.Owner;
            record.Object = Json.Encode(msg.Details);
            record.Type = "Device";
        }
    }
}